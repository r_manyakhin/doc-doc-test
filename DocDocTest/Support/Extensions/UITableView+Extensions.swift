//
//  UITableView+Extensions.swift
//  DocDocTest
//
//  Created by Роман Маняхин on 18.05.2018.
//  Copyright © 2018 Roman Manyakhin. All rights reserved.
//

import UIKit

extension UITableView {

  /**
   Registers a UITableViewCell for use in a UITableView.

   - parameter type: The type of cell to register.
   - parameter reuseIdentifier: The reuse identifier for the cell (optional).

   By default, the class name of the cell is used as the reuse identifier.
   */
  public func registerCell<T>(_ type: T.Type = T.self, withIdentifier reuseIdentifier: String = String(describing: T.self)) where T: UITableViewCell {
    register(T.self, forCellReuseIdentifier: reuseIdentifier)
  }

  /**
   Registers a UITableViewCell for use in a UITableView.

   - parameter type: The type of cell to register.
   - parameter reuseIdentifier: The reuse identifier for the cell (optional).

   By default, the class name of the cell is used as the reuse identifier and nibName.
   */
  public func registerCellFromNib<T>(_ type: T.Type = T.self, withIdentifier reuseIdentifier: String = String(describing: T.self)) where T: UITableViewCell {
    register(UINib(nibName: reuseIdentifier, bundle: nil), forCellReuseIdentifier: reuseIdentifier)
  }

  /**
   Dequeues a UITableViewCell for use in a UITableView.

   - parameter type: The type of the cell.
   - parameter reuseIdentifier: The reuse identifier for the cell (optional).

   - returns: A force-casted UITableViewCell of the specified type.

   By default, the class name of the cell is used as the reuse identifier.
   */
  public func dequeueCell<T>(_ type: T.Type = T.self, withIdentifier reuseIdentifier: String = String(describing: T.self)) -> T where T: UITableViewCell {
    guard let cell = dequeueReusableCell(withIdentifier: reuseIdentifier) as? T else {
      fatalError("Unknown cell type (\(T.self)) for reuse identifier: \(reuseIdentifier)")
    }
    return cell
  }

}

//
//  StoryboardMakeable+Extensions.swift
//  DocDocTest
//
//  Created by Роман Маняхин on 18.05.2018.
//  Copyright © 2018 Roman Manyakhin. All rights reserved.
//

import UIKit

protocol StoryboardMakeable {

  static var storyboardName: String { get }

  static var storyboardBundle: Bundle? { get }

  static var storyboardIdentifier: String? { get }

}

protocol BurgerController {}

extension StoryboardMakeable {

  static var storyboardIdentifier: String? { return nil }
  static var storyboardBundle: Bundle? { return nil }
  static var storyboardName: String { return "" }

  static func make() -> Self {
    let storyboard = UIStoryboard(name: storyboardName, bundle: storyboardBundle)

    if let storyboardIdentifier = storyboardIdentifier {
      return storyboard.instantiateViewController(withIdentifier: storyboardIdentifier) as! Self
    } else {
      return storyboard.instantiateInitialViewController() as! Self
    }
  }

}

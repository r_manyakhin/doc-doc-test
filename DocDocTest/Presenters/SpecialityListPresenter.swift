//
//  SpecialityListPresenter.swift
//  DocDocTest
//
//  Created by Роман Маняхин on 18.05.2018.
//  Copyright © 2018 Roman Manyakhin. All rights reserved.
//

import Foundation

class SpecialityListPresenter {

  weak private var specialityListView : SpecialityListMvpView?

  private var cityId: String = ""

  func attachView(_ view: SpecialityListMvpView) {
    specialityListView = view
  }

  func detachView() {
    specialityListView = nil
  }

  func setCityIdVariable(_ cityId: String) {
    self.cityId = cityId
  }

  func initTableController() {
    let sortPredicate = NSPredicate(format: "%K == %@", "cityId", cityId)

    specialityListView?.initTableController(sortPredicate)
  }

  func getSpecialityList() {
    DataProcessor.getSpecialityList(by: cityId)
  }
  
}

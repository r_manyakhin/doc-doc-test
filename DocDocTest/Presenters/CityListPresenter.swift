//
//  CityListPresenter.swift
//  DocDocTest
//
//  Created by Роман Маняхин on 18.05.2018.
//  Copyright © 2018 Roman Manyakhin. All rights reserved.
//

import Foundation

class CityListPresenter {

  weak private var cityListView : CityListMvpView?

  func attachView(_ view: CityListMvpView) {
    cityListView = view
  }

  func detachView() {
    cityListView = nil
  }

  func getCityList() {
    DataProcessor.getCityList()
  }

  func openSpecialityListPage(cityId: String) {
    cityListView?.openSpecialityListPage(cityId: cityId)
  }

}

//
//  CityListMvpView.swift
//  DocDocTest
//
//  Created by Роман Маняхин on 18.05.2018.
//  Copyright © 2018 Roman Manyakhin. All rights reserved.
//

import Foundation

protocol CityListMvpView: NSObjectProtocol {

  func openSpecialityListPage(cityId: String)
  
}

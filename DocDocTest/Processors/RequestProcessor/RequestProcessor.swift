//
//  RequestProcessor.swift
//  DocDocTest
//
//  Created by Роман Маняхин on 18.05.2018.
//  Copyright © 2018 Roman Manyakhin. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol ServerRequestProcessor: class {

  func getCityList(completion: @escaping (JSON) -> (), failure: @escaping (String) -> ())

  func getSpecialityList(by cityId: String, completion: @escaping (JSON) -> (), failure: @escaping (String) -> ())

}

class RequestProcessor: ServerRequestProcessor {

  static let shared: ServerRequestProcessor = RequestProcessor()

  private init() {}

  func baseResponse(
    _ url: URLConvertible,
    method: HTTPMethod = .get,
    parameters: Parameters? = nil,
    completion: @escaping (JSON) -> (),
    failure: @escaping (String) -> ()) {

    var headers = [String: String]()

    let user = "partner.13703"
    let password = "ZZdFmtJD"
    if let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8) {
      let base64Credentials = credentialData.base64EncodedString(options: [])

      headers = ["Authorization": "Basic \(base64Credentials)"]
    }

    Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: headers).validate().responseJSON(completionHandler: { response in
      switch response.result {
      case .success(let value):
        let json = JSON(value)

        completion(json)
      case .failure(let error):
        failure(error.localizedDescription)
      }
    })
  }

  func getCityList(completion: @escaping (JSON) -> (), failure: @escaping (String) -> ()) {

    if let url = RequestURL.city {
      baseResponse(url, method: .get, completion: completion, failure: failure)
    } else {
      failure("Error when init URL")
    }

  }

  func getSpecialityList(by cityId: String, completion: @escaping (JSON) -> (), failure: @escaping (String) -> ()) {

    if let url = RequestURL.speciality {
      baseResponse(url, method: .get, completion: completion, failure: failure)
    } else {
      failure("Error when init URL")
    }

  }

}

//
//  RequestURL.swift
//  DocDocTest
//
//  Created by Роман Маняхин on 18.05.2018.
//  Copyright © 2018 Roman Manyakhin. All rights reserved.
//

import Foundation

class RequestURL {

  static var apiURL: URL? {
    return URL(string: "https://api.docdoc.ru/public/rest/1.0.9/")
  }

  static var city: URL? {
    return apiURL?.appendingPathComponent("city")
  }

  static var speciality: URL? {
    return apiURL?.appendingPathComponent("speciality")
  }

}

//
//  SpecialityDBProcessor.swift
//  DocDocTest
//
//  Created by Роман Маняхин on 18.05.2018.
//  Copyright © 2018 Roman Manyakhin. All rights reserved.
//

import Foundation
import CoreData

import CocoaLumberjack

protocol SpecialityDataBaseProcessor: class {

  func getSpeciality(by id: String, cityId: String) -> Speciality?

  func getSpecialities(for cityId: String) -> [Speciality]

}

class SpecialityDBProcessor: SpecialityDataBaseProcessor {

  func getSpeciality(by id: String, cityId: String) -> Speciality? {
    let results = getSpecialities(for: cityId)

    return results.first(where: { entity in
      return entity.specId == id
    })
  }

  func getSpecialities(for cityId: String) -> [Speciality] {
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Speciality")

    let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
    fetchRequest.sortDescriptors = [sortDescriptor]

    let predicate = NSPredicate(format: "%K == %@", "cityId", "\(cityId)")
    fetchRequest.predicate = predicate

    do {
      let results = try CoreDataManager.instance.managedObjectContext.fetch(fetchRequest)

      if let cities = results as? [Speciality] {
        return cities
      }
    }
    catch {
      DDLogError("Fail execute getCities with error: \(error.localizedDescription)")
    }

    return []
  }

}

extension Speciality {

  convenience init() {
    self.init(entity: CoreDataManager.instance.entity(forName: "Speciality"), insertInto: CoreDataManager.instance.managedObjectContext)
  }

}

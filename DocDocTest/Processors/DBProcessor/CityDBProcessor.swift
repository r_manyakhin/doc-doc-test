//
//  CityDBProcessor.swift
//  DocDocTest
//
//  Created by Роман Маняхин on 18.05.2018.
//  Copyright © 2018 Roman Manyakhin. All rights reserved.
//

import Foundation
import CoreData

import CocoaLumberjack

protocol CityDataBaseProcessor: class {

  func getCity(by id: String) -> City?

  func getCities() -> [City]

}

class CityDBProcessor: CityDataBaseProcessor {

  func getCity(by id: String) -> City? {
    let results = getCities()

    return results.first(where: { entity in
      return entity.cityId == id
    })
  }

  func getCities() -> [City] {
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "City")

    let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
    fetchRequest.sortDescriptors = [sortDescriptor]

    do {
      let results = try CoreDataManager.instance.managedObjectContext.fetch(fetchRequest)

      if let cities = results as? [City] {
        return cities
      }
    }
    catch {
      DDLogError("Fail execute getCities with error: \(error.localizedDescription)")
    }

    return []
  }

}

extension City {

  convenience init() {
    self.init(entity: CoreDataManager.instance.entity(forName: "City"), insertInto: CoreDataManager.instance.managedObjectContext)
  }

}

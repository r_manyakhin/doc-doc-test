//
//  DBProcessor.swift
//  DocDocTest
//
//  Created by Роман Маняхин on 18.05.2018.
//  Copyright © 2018 Roman Manyakhin. All rights reserved.
//

import Foundation

protocol DataBaseProcessor {

  static var shared: DataBaseProcessor { get }

  var cityProcessor: CityDataBaseProcessor { get }
  var specialityProcessor: SpecialityDataBaseProcessor { get }

}

class DBProcessor: DataBaseProcessor {

  static let shared: DataBaseProcessor = DBProcessor()

  private init() {}

  let cityProcessor: CityDataBaseProcessor = CityDBProcessor()
  var specialityProcessor: SpecialityDataBaseProcessor = SpecialityDBProcessor()

}

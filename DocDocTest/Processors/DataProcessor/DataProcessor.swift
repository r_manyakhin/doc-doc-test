//
//  DataProcessor.swift
//  DocDocTest
//
//  Created by Роман Маняхин on 18.05.2018.
//  Copyright © 2018 Roman Manyakhin. All rights reserved.
//

import Foundation

import CocoaLumberjack

class DataProcessor {

  static let requestProcessor: ServerRequestProcessor = RequestProcessor.shared

  static let dbProcessor: DataBaseProcessor = DBProcessor.shared

  static func getCityList() {

    requestProcessor.getCityList(completion: { result in

      guard let cityList = result["CityList"].array else {
        DDLogError("Fail execute getCityList with error: no field CityList")

        return
      }

      for city in cityList {
        if let cityId = city["Id"].string, let cityName = city["Name"].string {
          let entity = dbProcessor.cityProcessor.getCity(by: cityId) ?? City()

          entity.cityId = cityId
          entity.name = cityName

          CoreDataManager.instance.saveContext()
        }
      }

    }, failure: { error in
      DDLogError("Fail execute getCityList with error: \(error)")
    })

  }

  static func getSpecialityList(by cityId: String) {

    requestProcessor.getSpecialityList(by: cityId, completion: { result in

      guard let specList = result["SpecList"].array else {
        DDLogError("Fail execute getSpecialityList with error: no field SpecList")

        return
      }

      for spec in specList {
        if let specId = spec["Id"].string, let specName = spec["Name"].string {
          let entity = dbProcessor.specialityProcessor.getSpeciality(by: specId, cityId: cityId) ?? Speciality()

          entity.specId = specId
          entity.cityId = cityId
          entity.name = specName

          CoreDataManager.instance.saveContext()
        }
      }

    }, failure: { error in
      DDLogError("Fail execute getSpecialityList with error: \(error)")
    })

  }

}

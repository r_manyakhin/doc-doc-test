//
//  SpecialityListViewController.swift
//  DocDocTest
//
//  Created by Роман Маняхин on 18.05.2018.
//  Copyright © 2018 Roman Manyakhin. All rights reserved.
//

import UIKit
import CoreData

import CocoaLumberjack

class SpecialityListViewController: UIViewController {

  // MARK: - Outlets

  @IBOutlet weak var table: UITableView!

  // MARK: - Variables

  private let presenter: SpecialityListPresenter = SpecialityListPresenter()

  private var tableController: NSFetchedResultsController<NSFetchRequestResult>?

  // MARK: - Controller Standard Methods

  override func viewDidLoad() {
    super.viewDidLoad()

    title = "Список специальностей"

    table.registerCellFromNib(LabelTableViewCell.self)

    table.delegate = self
    table.dataSource = self

    presenter.attachView(self)

    presenter.initTableController()

    presenter.getSpecialityList()
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }

}

// MARK: - UITableView Delegate & DataSource Extension

extension SpecialityListViewController: UITableViewDelegate, UITableViewDataSource {

  func numberOfSections(in tableView: UITableView) -> Int {
    return tableController?.sections?.count ?? 0
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return tableController?.sections?[section].numberOfObjects ?? 0
  }

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return LabelTableViewCell.height()
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueCell(LabelTableViewCell.self)

    if let object = tableController?.object(at: indexPath) as? Speciality {
      cell.fill(speciality: object)
    } else {
      DDLogError("Fail execute cellForRow for tableController with error: could not get speciality")
    }

    return cell
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
  }

}

// MARK: - NSFetchedResultsControllerDelegate Extension

extension SpecialityListViewController: NSFetchedResultsControllerDelegate {

  func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
    table.beginUpdates()
  }

  func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
    switch type {
    case .insert:
      table.insertSections(IndexSet(integer: sectionIndex), with: .none)
    case .delete:
      table.deleteSections(IndexSet(integer: sectionIndex), with: .none)
    default:
      break
    }
  }

  func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
    switch type {
    case .insert:
      if let newIndexPath = newIndexPath {
        table.insertRows(at: [newIndexPath], with: .top)
      }
    case .delete:
      if let indexPath = indexPath {
        table.deleteRows(at: [indexPath], with: .bottom)
      }
    case .update:
      if let indexPath = indexPath {
        table.reloadRows(at: [indexPath], with: .none)
      }
    case .move:
      if let indexPath = indexPath, let newIndexPath = newIndexPath {
        table.moveRow(at: indexPath, to: newIndexPath)
      }
    }
  }

  func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
    table.endUpdates()
  }

}

// MARK: - MvpView Extension

extension SpecialityListViewController: SpecialityListMvpView {

  func initTableController(_ sortPredicate: NSPredicate) {
    tableController = CoreDataManager.instance.fetchedResultsController(entityName: "Speciality", keyForSort: "name", withPredicate: sortPredicate)

    tableController?.delegate = self

    do {
      try tableController?.performFetch()
    } catch {
      DDLogError("Fail execute performFetch for tableController with error: \(error)")
    }
  }

}

// MARK: - Storyboard Makeable Extension

extension SpecialityListViewController: StoryboardMakeable {

  static var storyboardName: String { return "Main" }
  static var storyboardIdentifier: String? { return "SpecialityListViewController" }

  static func makeWith(cityId: String) -> SpecialityListViewController {
    let control = make()

    control.presenter.setCityIdVariable(cityId)

    return control
  }

}

//
//  LabelTableViewCell.swift
//  DocDocTest
//
//  Created by Роман Маняхин on 18.05.2018.
//  Copyright © 2018 Roman Manyakhin. All rights reserved.
//

import UIKit

class LabelTableViewCell: UITableViewCell {

  // MARK: - Outlets

  @IBOutlet weak var label: UILabel!

  // MARK: - Cell Standard Methods

  override func awakeFromNib() {
    super.awakeFromNib()
  }

  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }

  // MARK: - Height

  class func height() -> CGFloat {
    return 44
  }

  // MARK: - Cell Methods

  func fill(city: City) {
    label.text = city.name
  }

  func fill(speciality: Speciality) {
    label.text = speciality.name
  }

}
